﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace aksrProj
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        // LOGIN
        private void buttonLogin_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxUsername.Text) && string.IsNullOrEmpty(textBoxPassword.Text))
            {
                MessageBox.Show("Greska pri logovanju. Potrebno je popuniti sva polja za unos.");
            }
            else
            {
                APIController apicontroller = new APIController();
                if (apicontroller.userAuth(textBoxUsername.Text, textBoxPassword.Text))
                {
                    Console.WriteLine("SUCCESS");
                    MessageBox.Show("Dobro Dosli!");
                    Rezervacije_Letova letovi = new Rezervacije_Letova();
                    letovi.Show();
                    this.Close();
                }
                else
                {
                    Console.WriteLine("FAILED");
                    textBoxUsername.Clear();
                    textBoxPassword.Clear();
                    MessageBox.Show("Greska pri logovanju. Pogresni podaci za login, proverite svoj username i password i pokusajte ponovo.");
                }
            }
        }

        // REGISTRACIJA
        private void buttonRegistracija_Click(object sender, RoutedEventArgs e)
        {
            Registracija regstracija = new Registracija();
            regstracija.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            regstracija.ShowDialog();
        }
    }
}
