﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Data.Objects;
using aksrProj.Properties;


namespace aksrProj
{
    /// <summary>
    /// Interaction logic for Rezervacije_Letova.xaml
    /// </summary>
    public partial class frmLetovi : Window
    {
        public frmLetovi()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            LoadFlights();

            System.Console.WriteLine(ClassKorisnik.korisnickoImeS);
            System.Console.WriteLine(ClassKorisnik.jmbgS);
            System.Console.WriteLine(ClassKorisnik.prezimeS);
            System.Console.WriteLine(ClassKorisnik.imeS);
            System.Console.WriteLine(ClassKorisnik.datumRodjenjaS);
            System.Console.WriteLine(ClassKorisnik.emailS);
            System.Console.WriteLine(ClassKorisnik.privilegijaS);
            System.Console.WriteLine(ClassKorisnik.statusS);

            if (Settings.Default.privilegija == 1)
            {
                buttonRezervacije.Visibility = Visibility.Visible;
                buttonKorisnici.Visibility = Visibility.Visible;
                buttonUnesiLet.Visibility = Visibility.Visible;

            }
            else
            {
                buttonRezervacije.Visibility = Visibility.Hidden;
                buttonKorisnici.Visibility = Visibility.Hidden;
                buttonUnesiLet.Visibility = Visibility.Hidden;
            }
        }

        private void LoadFlights()
        {

        }

        private void dataGridRezervacije_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Console.WriteLine("Changed");
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
 
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonUnesiLet_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiLet noviLet = new frmUnesiLet();
            noviLet.ShowDialog();
        }

        private void buttonUnesiAerodrom_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiAerodrom frmNoviAerodrom = new frmUnesiAerodrom();
            frmNoviAerodrom.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviAerodrom.ShowDialog();
        }

        private void buttonDodajPrevozioca_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiPrevozioca frmNoviPrevozilac = new frmUnesiPrevozioca();
            frmNoviPrevozilac.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviPrevozilac.ShowDialog();
        }

        private void buttonDodajAvion_Click_1(object sender, RoutedEventArgs e)
        {
            frmUnesiAvion frmNoviAvion = new frmUnesiAvion();
            frmNoviAvion.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviAvion.ShowDialog();
        }

        private void buttonIzadji_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonKorisnici_Click(object sender, RoutedEventArgs e)
        {
            frmKorisnici frmKorisnici = new frmKorisnici();
            frmKorisnici.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmKorisnici.ShowDialog();
        } 
    }
}
