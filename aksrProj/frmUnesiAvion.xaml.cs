﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace aksrProj
{
    /// <summary>
    /// Interaction logic for UnesiAvion.xaml
    /// </summary>
    public partial class frmUnesiAvion : Window
    {
        public frmUnesiAvion()
        {
            InitializeComponent();
            BindPrevozilacComboBox(comboBoxPrevozilac);
        }

        private void buttonZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonDodajNoviAvion_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(comboBoxPrevozilac.Text))
            {
                MessageBox.Show("Molimo odaberite prevozioca.");
            }
            else if (string.IsNullOrEmpty(tbAvionReg.Text))
            {
                MessageBox.Show("Molimo unesite registracioni broj vazduhoplova.");
            }
            else if (string.IsNullOrEmpty(tbBrojSedista.Text))
            {
                MessageBox.Show("Molimo unesite broj sedista.");
            }
            else
            {
                APIController apiController = new APIController();

                if (apiController.addNewAircraft(tbAvionReg.Text, tbBrojSedista.Text, comboBoxPrevozilac.SelectedValue.ToString()))
                {
                    MessageBox.Show("Vazduhoplov uspesno dodat.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Greska prilikom dodavanja vazduhoplova.");
                }
            }
        }

        public void BindPrevozilacComboBox(ComboBox comboBoxName)
        {
            SqlConnection conn = new SqlConnection("server=.\\SQLEXPRESS;initial catalog=AKSR_STAGED;trusted_connection=true");
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            try
            {
                conn.Open();
                da.SelectCommand = new SqlCommand("Select prevozilac_id, telefon, ime, status FROM prevozioci", conn);
                da.Fill(ds, "ime");
                conn.Close();
                comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["ime"].ToString();
                comboBoxName.SelectedValuePath = ds.Tables[0].Columns["prevozilac_id"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
