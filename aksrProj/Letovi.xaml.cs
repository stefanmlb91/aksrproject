﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Data.Objects;
using aksrProj.Properties;


namespace aksrProj
{
    /// <summary>
    /// Interaction logic for Rezervacije_Letova.xaml
    /// </summary>
    public partial class Rezervacije_Letova : Window
    {
        public Rezervacije_Letova()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            LoadFlights();

            System.Console.WriteLine(Korisnik.korisnickoImeS);
            System.Console.WriteLine(Korisnik.jmbgS);
            System.Console.WriteLine(Korisnik.prezimeS);
            System.Console.WriteLine(Korisnik.imeS);
            System.Console.WriteLine(Korisnik.datumRodjenjaS);
            System.Console.WriteLine(Korisnik.emailS);
            System.Console.WriteLine(Korisnik.privilegijaS);
            System.Console.WriteLine(Korisnik.statusS);

            if (Settings.Default.privilegija == 1)
            {
                buttonRezervacije.Visibility = Visibility.Visible;
                buttonKorisnici.Visibility = Visibility.Visible;
                buttonUnesiLet.Visibility = Visibility.Visible;

            }
            else
            {
                buttonRezervacije.Visibility = Visibility.Hidden;
                buttonKorisnici.Visibility = Visibility.Hidden;
                buttonUnesiLet.Visibility = Visibility.Hidden;
            }
        }

        private void LoadFlights()
        {
            /*
            DataClasses1DataContext dc = new DataClasses1DataContext();
            var letovi = from flight in dc.letovis
                         where flight.status == 1
                         select new { flight.let_id, flight.prevozioci.ime, flight.avion_id, flight.aerodrom_polaska_id, flight.aerodrom_dolaska_id, flight.datum_leta };
            dataGridRezervacije.ItemsSource = letovi;
             * */
        }

        private void dataGridRezervacije_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Console.WriteLine("Changed");
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataClasses1DataContext dataContext = new DataClasses1DataContext();
                Let letRed = dataGridRezervacije.SelectedItem as Let;
                string m = letRed.letID;
                var let = (from flight in dataContext.letovis
                           where flight.let_id == letRed.letID
                           select flight) as Let;

                let.letID = letRed.letID;
                let.prevozilacID = letRed.prevozilacID;
                let.avionID = letRed.avionID;
                let.aerodromPolaska = letRed.aerodromPolaska;
                let.aerodromDolaska = letRed.aerodromDolaska;
                let.datumLeta = letRed.datumLeta;
                let.status = letRed.status;
                dataContext.SubmitChanges();
                MessageBox.Show("Row Updated Successfully.");
                LoadFlights();

            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
                return;
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            /*
            CustomersDataContext cd = new CustomersDataContext();
            Customer customerRow = MyDataGrid.SelectedItem as Customer;
            var customer = (from p in cd.Customers
                            where p.CustomerID == customerRow.CustomerID
                            select p).Single();
            cd.Customers.DeleteOnSubmit(customer);
            cd.SubmitChanges();
            MessageBox.Show("Row Deleted Successfully.");
            LoadCustomers();
             * */
        }

        private void buttonUnesiLet_Click(object sender, RoutedEventArgs e)
        {
            Unesi_Let noviLet = new Unesi_Let();
            noviLet.ShowDialog();
        }

        private void buttonUnesiAerodrom_Click(object sender, RoutedEventArgs e)
        {
            UnesiAerodrom frmNoviAerodrom = new UnesiAerodrom();
            frmNoviAerodrom.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviAerodrom.ShowDialog();
        }

        private void buttonDodajPrevozioca_Click(object sender, RoutedEventArgs e)
        {
            UnesiPrevozioca frmNoviPrevozilac = new UnesiPrevozioca();
            frmNoviPrevozilac.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviPrevozilac.ShowDialog();
        }

        private void buttonDodajAvion_Click_1(object sender, RoutedEventArgs e)
        {
            UnesiAvion frmNoviAvion = new UnesiAvion();
            frmNoviAvion.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviAvion.ShowDialog();
        } 
    }
}
