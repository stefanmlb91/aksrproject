﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;

namespace aksrProj
{
    /// <summary>
    /// Interaction logic for Korisnici.xaml
    /// </summary>
    public partial class frmKorisnici : Window
    {
        AKSREntities context = new AKSREntities();

        bool isInsertMode = false;
        bool isBeingEdited = false;

        public frmKorisnici()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dgKorisnici.ItemsSource = GetKorisnikList();
        }

        private ObservableCollection<korisnici> GetKorisnikList()
        {
            var list = from e in context.korisnicis select e;
            return new ObservableCollection<korisnici>(list);
        }

        private void dgEmp_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            korisnici korisnik = new korisnici();
            korisnici kor = e.Row.DataContext as korisnici;

            if (isInsertMode)
            {
                var InsertRecord = MessageBox.Show("Zelite li da dodate " + kor.ime + " kao novog korisnika?", "Potvrdi", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (InsertRecord == MessageBoxResult.Yes)
                {
                    korisnik.jmbg = kor.jmbg;
                    korisnik.ime = kor.ime;
                    korisnik.prezime = kor.prezime;
                    korisnik.status = kor.status;
                    korisnik.privilegija = kor.privilegija;
                    korisnik.datum_rodjenja = kor.datum_rodjenja;
                    korisnik.username = kor.username;
                    korisnik.email = kor.email;

                    context.korisnicis.Add(korisnik);
                    context.SaveChanges();
                    dgKorisnici.ItemsSource = GetKorisnikList();

                    MessageBox.Show(korisnik.ime + " " + korisnik.prezime + " je uspesno dodat!", "Dodavanja korisnika", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                    dgKorisnici.ItemsSource = GetKorisnikList();
            }
            context.SaveChanges();
        }

        private void dgEmp_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && !isBeingEdited)
            {
                var grid = (DataGrid)sender;
                if (grid.SelectedItems.Count > 0)
                {
                    var Res = MessageBox.Show("Da li ste sigurni da zelite da obrisete " + grid.SelectedItems.Count + " Korisnike?", "Brisanje zapisa", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                    if (Res == MessageBoxResult.Yes)
                    {
                        foreach (var row in grid.SelectedItems)
                        {
                            korisnici korisnik = row as korisnici;
                            context.korisnicis.Remove(korisnik);
                        }
                        context.SaveChanges();
                        MessageBox.Show(grid.SelectedItems.Count + " korisnika je obrisano!!");
                    }
                    else
                        dgKorisnici.ItemsSource = GetKorisnikList();
                }
            }
        }

        private void dgEmp_AddingNewItem(object sender, AddingNewItemEventArgs e)
        {
            isInsertMode = true;
        }

        private void dgEmp_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            isBeingEdited = true;
        }

        private void buttonZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
