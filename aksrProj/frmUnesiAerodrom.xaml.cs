﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace aksrProj
{
    /// <summary>
    /// Interaction logic for UnesiAerodrom.xaml
    /// </summary>
    public partial class frmUnesiAerodrom : Window
    {
        public frmUnesiAerodrom()
        {
            InitializeComponent();
        }

        private void buttonZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonDodajAerodrom_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbAerodromID.Text))
            {
                MessageBox.Show("Molimo unesite IATA kod aerodrom.");
            }
            else if (string.IsNullOrEmpty(tbAerodromIme.Text))
            {
                MessageBox.Show("Molimo unesite ime aerodroma.");
            }
            else if (string.IsNullOrEmpty(tbAerodromGrad.Text))
            {
                MessageBox.Show("Molimo unesite grad aerodroma.");
            } 
            else 
            {
                APIController apiController = new APIController();

                if (apiController.createAirport(tbAerodromID.Text, tbAerodromIme.Text, tbAerodromGrad.Text))
                {
                    MessageBox.Show("Aerodrom uspesno dodat.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Greska prilikom unosa aerodroma.");
                }
            }

        }
    }
}
