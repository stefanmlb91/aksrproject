﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using aksrProj.Properties;

namespace aksrProj
{
    /// <summary>
    /// Interaction logic for Registracija.xaml
    /// </summary>
    public partial class frmRegistracija : Window
    {
        public frmRegistracija()
        {
            InitializeComponent();

            if (Settings.Default.privilegija == 0 || ClassKorisnik.privilegijaS == 0)
            {
                privilegijaLabel.Visibility = Visibility.Hidden;
                textBoxPrivilegija.Visibility = Visibility.Hidden;
            }
            else
            {
                privilegijaLabel.Visibility = Visibility.Visible;
                textBoxPrivilegija.Visibility = Visibility.Visible;
            }
        }

        private void buttonUnesi_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxIme.Text) || string.IsNullOrEmpty(textBoxPrezime.Text) || string.IsNullOrEmpty(textBoxJmbg.Text) || string.IsNullOrEmpty(textBoxEmail.Text) || string.IsNullOrEmpty(textBoxUsername.Text) || string.IsNullOrEmpty(textBoxPassword.Text))
            {
                MessageBox.Show("Greska pri registraciji.Potrebno je popuniti sva polja za unos.");
            }
            else
            {
                APIController apiController = new APIController();
                if (Settings.Default.privilegija == 0 || ClassKorisnik.privilegijaS == 0) {
                    if (apiController.createAccount(textBoxIme.Text, textBoxPrezime.Text, textBoxJmbg.Text, textBoxEmail.Text, textBoxUsername.Text, textBoxPassword.Text, datePickerDatumRodjenja.Text))
                    {
                        Console.WriteLine("Account Created");
                        MessageBox.Show("Nalog uspesno napravljen");
                        this.Close();
                    } 
                    else
                    {
                        MessageBox.Show("Greska pri registraciji. Vec postoji korisnik sa unetim podacima");
                        Console.WriteLine("Failed To Create Account");
                    }
                } 
                else 
                {
                    if (apiController.createAccountAdmin(textBoxIme.Text, textBoxPrezime.Text, textBoxJmbg.Text, textBoxEmail.Text, textBoxUsername.Text, textBoxPassword.Text, datePickerDatumRodjenja.Text, Convert.ToInt32(textBoxPrivilegija.Text)))
                    {
                        Console.WriteLine("Account Created");
                        MessageBox.Show("Nalog uspesno napravljen");
                        this.Close();
                    } 
                    else
                    {
                        MessageBox.Show("Greska pri registraciji. Vec postoji korisnik sa unetim podacima");
                        Console.WriteLine("Failed To Create Account");
                    }
                }

            }
        }
    }
}
