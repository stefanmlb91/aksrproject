﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace aksrProj
{
    /// <summary>
    /// Interaction logic for Unesi_Let.xaml
    /// </summary>
    public partial class frmUnesiLet : Window
    {
        public frmUnesiLet()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            BindPrevozilacComboBox(comboBoxPrevozilac);
            BindAerodromComboBox(comboBoxAerodromPolaska);
            BindAerodromComboBox(comboBoxAerodromDolaska);
        }
        
        private void buttonDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxLetID.Text))
            {
                MessageBox.Show("Molimo unesite ID leta.");
            } 
            else if (string.IsNullOrEmpty(comboBoxAerodromPolaska.Text))
            {
                MessageBox.Show("Molimo odaberite aerodrom polaska leta.");
            } 
            else if (string.IsNullOrEmpty(comboBoxAerodromDolaska.Text))
            {
                MessageBox.Show("Molimo odaberite aerodrom dolaska leta.");
            } 
            else if (datePickerDatumPolaska.SelectedDate == null)
            {
                MessageBox.Show("Molimo odaberite datum leta.");
            } 
            else if (string.IsNullOrEmpty(comboBoxPrevozilac.Text))
            {
                MessageBox.Show("Molimo odaberite prevozioca.");
            } 
            else if (string.IsNullOrEmpty(comboBoxAvioReg.Text))
            {
                MessageBox.Show("Molimo odaberite vazduhoplov.");
            } 
            else
            {
                APIController apiController = new APIController();
                if (apiController.createFlight(textBoxLetID.Text, comboBoxPrevozilac.SelectedValue.ToString(), comboBoxAvioReg.SelectedValue.ToString(), comboBoxAerodromPolaska.SelectedValue.ToString(), comboBoxAerodromDolaska.SelectedValue.ToString(), datePickerDatumPolaska.Text))
                {
                    MessageBox.Show("Let uspesno kreiran.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Neuspesno kreiranje leta.");
                }
            }
        }

        public void BindPrevozilacComboBox(ComboBox comboBoxName)
        {
            SqlConnection conn = new SqlConnection("server=.\\SQLEXPRESS;initial catalog=AKSR_STAGED;trusted_connection=true");
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            try
            {
                conn.Open();
                da.SelectCommand = new SqlCommand("Select prevozilac_id, telefon, ime, status FROM prevozioci", conn);
                da.Fill(ds, "ime");
                conn.Close();
                comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["ime"].ToString();
                comboBoxName.SelectedValuePath = ds.Tables[0].Columns["prevozilac_id"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void BindAvionRegComboBox(ComboBox comboBoxName)
        {
            SqlConnection conn = new SqlConnection("server=.\\SQLEXPRESS;initial catalog=AKSR_STAGED;trusted_connection=true");
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            try
            {
                conn.Open();
                da.SelectCommand = new SqlCommand("Select avion_id, broj_sedista, prevozilac_id FROM avioni WHERE prevozilac_id = " + comboBoxPrevozilac.SelectedValue + "", conn);
                da.Fill(ds, "avion_id");
                conn.Close();
                comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["avion_id"].ToString();
                comboBoxName.SelectedValuePath = ds.Tables[0].Columns["avion_id"].ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public void BindAerodromComboBox(ComboBox comboBoxName)
        {
            SqlConnection conn = new SqlConnection("server=.\\SQLEXPRESS;initial catalog=AKSR_STAGED;trusted_connection=true");
            SqlDataAdapter da = new SqlDataAdapter();
            DataSet ds = new DataSet();
            try
            {
                conn.Open();
                da.SelectCommand = new SqlCommand("Select aerodrom_id, ime, grad FROM aerodromi", conn);
                da.Fill(ds, "aerodrom_id");
                conn.Close();
                comboBoxName.ItemsSource = ds.Tables[0].DefaultView;
                comboBoxName.DisplayMemberPath = ds.Tables[0].Columns["aerodrom_id"].ToString();
                comboBoxName.SelectedValuePath = ds.Tables[0].Columns["aerodrom_id"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void comboBoxPrevozilac_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BindAvionRegComboBox(comboBoxAvioReg);
        }

        private void buttonDodajPrevozioca_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiPrevozioca frmNoviPrevozilac = new frmUnesiPrevozioca();
            frmNoviPrevozilac.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviPrevozilac.Closed += ChildWindowClosed;
            frmNoviPrevozilac.ShowDialog();
        }

        private void buttonDodajAerodrom_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiAerodrom frmNoviAerodrom = new frmUnesiAerodrom();
            frmNoviAerodrom.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviAerodrom.Closed += ChildWindowClosed;
            frmNoviAerodrom.ShowDialog();
        }

        private void buttonDodajAvion_Click(object sender, RoutedEventArgs e)
        {
            frmUnesiAvion frmNoviAvion = new frmUnesiAvion();
            frmNoviAvion.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            frmNoviAvion.Closed += ChildWindowClosed;
            frmNoviAvion.ShowDialog();
        }

        private void ChildWindowClosed(object sender, EventArgs e)
        {
            ((Window)sender).Closed -= ChildWindowClosed;

            BindAerodromComboBox(comboBoxAerodromPolaska);
            BindAerodromComboBox(comboBoxAerodromDolaska);
            BindPrevozilacComboBox(comboBoxPrevozilac);
        }

        private void buttonZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


    }
}
