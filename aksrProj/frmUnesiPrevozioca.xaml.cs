﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace aksrProj
{
    /// <summary>
    /// Interaction logic for UnesiPrevozioca.xaml
    /// </summary>
    public partial class frmUnesiPrevozioca : Window
    {
        public frmUnesiPrevozioca()
        {
            InitializeComponent();
        }

        private void buttonZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonDodajPrevozioca_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbImePrevozioca.Text))
            {
                MessageBox.Show("Molimo unesite ime prevozioca.");
            }
            else if (string.IsNullOrEmpty(tbTelefonPrevozioca.Text))
            {
                MessageBox.Show("Molimo unesite telefon prevozioca.");
            }
            else
            {
                APIController apiController = new APIController();

                if (apiController.addNewCompany(tbImePrevozioca.Text, tbTelefonPrevozioca.Text))
                {
                    MessageBox.Show("Kompanija uspesno dodata.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Greska prilikom unosa kompanije.");
                }
            }
        }
    }
}
